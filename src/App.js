import React from 'react';
import './App.css';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';

import {setUserInfo} from './redux/actions/users'

// import TacosPage from './components/TacosPage';

function App(props) {
  console.log(props);

  const handleOnClick = (type) => {
    if(type === 'name'){
      props.setUserInfo({name: 'new bigboss'})
    }
    else if(type==='age'){
      props.setUserInfo({age: 25})
    }
    else{
      props.setUserInfo({gender: 'male'}) 
    }
  }

  return (
    <>
      <button onClick={()=>handleOnClick('name')}>Add name</button>
      <button onClick={()=>handleOnClick('age')}>Add age</button>
      <button onClick={()=>handleOnClick('gender')}>Add gender</button>
    </>
  );
}

const mapStateToProps = state => {
  return{
      users: state.users
    }
}

const mapDispatchToProps = dispatch => {
  return bindActionCreators({
    setUserInfo
  }, dispatch)
}

export default connect(mapStateToProps)(App);
