import React from 'react';
import {Badge, Button} from 'react-bootstrap';
import tacos from '../images/tacos7.png';
import atarah from '../images/atarah.jpg';
import mommy from '../images/mommy.jpg';

function TacosPage(){
	return(
			<>
				<div className="tacos-div">
					<div><img src={tacos} alt="tacos" className="tacos1" /></div>
					<div><img src={tacos} alt="tacos" className="tacos2" /></div>
					<div><img src={tacos} alt="tacos" className="tacos3" /></div>
					<div><img src={tacos} alt="tacos" className="tacos4" /></div>
					<div><img src={tacos} alt="tacos" className="tacos5" /></div>
					<div><img src={tacos} alt="tacos" className="tacos6" /></div>
					<div><img src={tacos} alt="tacos" className="tacos7" /></div>
					<div><img src={tacos} alt="tacos" className="tacos8" /></div>
				</div>
			</>
		);
}

export default TacosPage;