import { SET_USER_INFO } from '../types';

function setUserInfo(payload){
	return(
			{
				type: SET_USER_INFO,
				payload: payload
			}
		)
}
